import bot, json, os

from database import database
from decorators import rule, admin, command
from time import sleep
from helper import sanitize, getRealCommand

from constants import *


@rule
@admin
@command('update')
def update(message):
    force = False
    if len(message.arguments) > 1:
        if message.arguments[1].lower() == '-f':
            force = True

    text = "Pulling from git..."
    req = bot.sendMessage(chat_id = message.chat.id,
                          text = '<pre>{0}</pre>'.format(text),
                          reply_to_message_id = message.id,
                          parse_mode = 'HTML')

    msgId = req['result']['message_id']

    os.system('cd {0} && git pull > /tmp/message'.format(DIRECTORY))

    with open('/tmp/message','r') as f:
        result = f.read().strip()
        text += '\n\n' +  result

    sleep(1)

    edit(message.chat.id, msgId, text)

    if not force:
        if 'up-to-date' in result.lower(): return
        if 'up to date' in result.lower(): return
        if 'conflict' in result.lower(): return

    text += '\n\nRestarting bot...\n'
    edit(message.chat.id, msgId, text)

    os.system('kill -KILL `pgrep -f horseshoe | head -n 1`')

@rule
@admin
@command('reboot')
def reboot(message):
    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>Ok</pre>',
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

    os.system('kill -KILL `pgrep -f horseshoe | head -n 1`')

@rule
@admin
@command('db-dump')
def dbdump(message):
    if len(message.arguments) > 1:
        value = database.get(message.arguments[1])
    else:
        value = database.values

    msg = json.dumps(value, indent=1)
    msg = sanitize(msg)

    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>{0}</pre>'.format(msg),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@rule
@admin
@command('db-remove')
def dbremove(message):
    if len(message.arguments) < 2: return

    database.pop(message.arguments[1])

    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>Ok</pre>',
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@rule
@admin
@command('db-keys')
def dbkeys(message):
    msg = ''

    for key in database.values:
        msg += key + '\n'

    msg = sanitize(msg)

    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>{0}</pre>'.format(msg),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@rule
@admin
@command('db-clean')
def dbclean(message):
    before = database.size()

    database.pop('shutdown-until')
    database.pop('shutdown-muted')
    database.pop('pics-nexttime')

    database.set('spam-muted', {})
    database.set('spam-prevMsgs', {})

    aliases = database.get('aliases', {})

    for alias in dict(aliases):
        try:
            realCommand = getRealCommand(alias)
        except RecursionError:
            realCommand = None

        if realCommand is None:
            aliases.pop(alias)

    database.save()
    after = database.size()
    cleaned = before-after


    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>Cleaned {0:,} bits ({1:.1f}%)</pre>'.format(cleaned, 100*cleaned/before),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@rule
@admin
@command('db-size')
def dbsize(message):
    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>{:,}</pre>'.format(database.size()),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')
@rule
@admin
@command('last-response')
def lastResponse(message):
    try:
        method = message.arguments[1]
    except IndexError:
        method = 'last'

    try:
        response = bot.lastResponses[method]
        response = json.dumps(response, indent=1)
    except KeyError:
        response = 'Error'

    bot.sendMessage(chat_id = message.chat.id,
                    reply_to_message_id = message.id,
                    text = '<pre>{0}</pre>'.format(response),
                    parse_mode = 'HTML')



def edit(chatId, msgId, text):
    bot.editMessageText(chat_id = chatId,
                        message_id = msgId,
                        text = '<pre>{0}</pre>'.format(text),
                        parse_mode = 'HTML')

def inform():
    info = database.get('reboot-message')
    if not info: return

    text = info['text'] + 'Done'

    edit(info['chat'], info['id'], text)
