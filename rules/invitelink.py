import bot, time, json

from decorators import rule, admin, command, noParam, interceptor
from constants import *
from helper import minutesDisplay

from database import database

DEFAULT_MESSAGE = '<b>Join the Horseshoe</b>\n\nPress the button to get an invite link to the group chat.'
MESSAGE_WITH_LINK = '<b>Join the Horseshoe</b>\n\n<b>Click the link to join the group chat: </b>\n\n{0}\n\nThis link will only be valid for 10 seconds.'

KEYBOARD = json.dumps({'inline_keyboard' : [[{'text': 'Get invite link', 'callback_data': 'inviteLink'}]]})

lastRequests = {}

@rule
@admin
@command('postinvitelink')
@noParam
def postInviteLink():
    bot.sendMessage(chat_id = INVITE_LINK_CHANNEL,
                    text = DEFAULT_MESSAGE,
                    parse_mode = 'HTML',
                    reply_markup = KEYBOARD)


@rule('callback_query')
@command('inviteLink')
def genInviteLink(callbackQuery):
    userId = callbackQuery.user.id


    if userId in lastRequests:
        delta = time.time() - lastRequests[userId]

        if delta < 60:
            bot.answerCallbackQuery(callback_query_id = callbackQuery.id,
                                    text = "Try again in {0}.".format(minutesDisplay(1 - delta / 60.0)),
                                    show_alert = True)
            return False

    if userId == 556762656:
        bot.unbanChatMember(chat_id = CHAT_ID,
                            user_id = 172033414)

    lastRequests[userId] = time.time()

    req = bot.exportChatInviteLink(chat_id = CHAT_ID)

    link = req['result']

    bot.editMessageText(chat_id = callbackQuery.message.chat.id,
                        message_id = callbackQuery.message.id,
                        text = MESSAGE_WITH_LINK.format(link),
                        parse_mode = 'HTML')

    bot.sendMessage(chat_id = DOM_ID,
                    text = '{0} requested an invite link'.format(callbackQuery.user.display),
                    parse_mode = 'HTML')

    time.sleep(10)

    bot.exportChatInviteLink(chat_id = CHAT_ID)

    bot.editMessageText(chat_id = callbackQuery.message.chat.id,
                        message_id = callbackQuery.message.id,
                        text = DEFAULT_MESSAGE,
                        parse_mode = 'HTML',
                        reply_markup = KEYBOARD)

    knownUsers = database.get('known-users', {})


    if userId not in knownUsers:
        knownUsers[str(userId)] = {
            'lastSeen': time.time(),
            'subscribeCount': 0
        }

    database.save()

    kickIfSubscribed(userId)

def getSubscriberCount():
    req = bot.getChatMembersCount(chat_id = INVITE_LINK_CHANNEL)

    return req['result']

def isSubscribed(userId):
    req = bot.getChatMember(chat_id = INVITE_LINK_CHANNEL,
                            user_id = userId)

    if not req['ok']: return False
    if req['result']['status'] == 'member': return True

    return False

def kickIfSubscribed(userId):
    if isSubscribed(userId):
        # For some reason, unban acts as ban + immediate unban
        bot.unbanChatMember(chat_id = INVITE_LINK_CHANNEL,
                            user_id = userId)

        bot.sendMessage(chat_id = DOM_ID,
                        text = 'Kicked <a href="tg://user?id={0}">this person</a> from the invite link channel'.format(userId),
                        parse_mode = 'HTML')

        knownUsers = database.get('known-users')

        knownUsers[str(userId)]['subscribeCount'] += 1
        knownUsers[str(userId)]['lastSeen'] = int(time.time())

        return True

    return False

@interceptor
def kickSubscriberRoutine(message):
    knownUsers = database.get('known-users', {})
    if message.user.hash not in knownUsers:
        knownUsers[message.user.hash] = {
            'lastSeen': message.date,
            'subscribeCount': 0
        }
        database.save()

    if message.id % 10 != 0: return False

    count = getSubscriberCount()

    if count == 1: return False

    sortedKnownUsers = sorted(knownUsers.items(), key=lambda i: (i[1]['subscribeCount'], i[1]['lastSeen']), reverse=True)

    for userId, attributes in sortedKnownUsers:
        if kickIfSubscribed(userId): count -= 1

        if count == 1: return True

    bot.sendMessage(chat_id = DOM_ID,
                    text = 'Unknown user subscribed to @horseshoe')