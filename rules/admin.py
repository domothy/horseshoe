import json, bot

from decorators import rule, chat, admin, command, reply
from helper import minutesDisplay, sanitize
from database import database

from constants import *

@rule
@chat
@admin
@command('ping')
def ping(message):
    bot.sendMessage(chat_id = CHAT_ID,
                    text = '<pre>Pong</pre>',
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@rule
@chat
@admin
@reply
@command('info')
def info(message):
    parent = message.reply_to_message.raw

    prettyJson = json.dumps(parent, indent=1)
    prettyJson = sanitize(prettyJson)

    bot.sendMessage(chat_id = CHAT_ID,
                    text='<pre>{0}</pre>'.format(prettyJson),
                    parse_mode = 'HTML',
                    reply_to_message_id = message.id)

@rule
@chat
@admin
@reply
@command('whois')
def whois(message):
    user = message.reply_to_message.user

    msg  = '- <b>ID</b>: <a href="tg://user?id={0}">{0}</a>\n'.format(user.id)
    msg += '- <b>First name</b>: {0}\n'.format(sanitize(user.first_name))

    if user.last_name:
        msg += '- <b>Last name</b>: {0}\n'.format(sanitize(user.last_name))
    if user.username:
        msg += '- <b>Username</b>: @{0}\n'.format(sanitize(user.username))

    bot.sendMessage(chat_id = CHAT_ID,
                    text=msg,
                    parse_mode = 'HTML',
                    reply_to_message_id = message.id)

@rule
@chat
@admin
@command('shutitdown')
def shutitdown(message):
    try:
        duration = float(message.arguments[1])
    except IndexError or ValueError:
        duration = 5.0

    msg = '<pre>The horseshoe is on lockdown for {0}.</pre>'.format(minutesDisplay(duration))

    if duration <  1: duration = 1
    if duration < 60: duration = 60

    bot.sendMessage(chat_id = CHAT_ID,
                    text=msg,
                    parse_mode = 'HTML')

    database.set('shutdown-until', message.date + duration * 60)

@rule
@chat
@admin
@command('unlock')
def unlock(message):
    database.set('shutdown-until', 0)

    mutedUsers = database.get('shutdown-muted', [])
    for userId in mutedUsers:
        bot.restrictChatMember(chat_id = CHAT_ID,
                               user_id = userId,
                               can_send_messages = True,
                               can_send_media_messages = True,
                               can_send_other_messages = True,
                               can_add_web_page_previews = True,
                               until_date = 0)

    database.set('shutdown-muted', [])

    bot.sendMessage(chat_id = CHAT_ID,
                    text = '<pre>The horseshoe is no longer on lock down.</pre>',
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@rule
@chat
@admin
@command('setqotcount')
def setqotcount(message):
    try:
        newCount = int(message.arguments[1])
    except:
        return False

    database.set('qotcount', newCount)

@rule
@chat
@admin
@reply
@command('nokeyboard')
def killkeyboard(message):
    parent = message.reply_to_message

    r = bot.editMessageText(chat_id = CHAT_ID,
                            message_id = parent.id,
                            text = parent.text,
                            parse_mode = 'HTML')

    print(r)