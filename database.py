import json

from constants import *

class Database:
    def __init__(self):
        self.values = {}
        self.load()

    def get(self, name, default=None):
        try:
            return self.values[name]
        except KeyError:
            self.values[name] = default

            return default

    def set(self, name, value):
        self.values[name] = value
        self.save()

    def save(self):
        json.dump(self.values, open(DB_FILE, 'w'), indent=1)

    def load(self):
        try:
            self.values = json.load(open(DB_FILE, 'r'))
        except: pass

    def pop(self, key):
        try:
            v = self.values.pop(key)
            self.save()
            return v
        except KeyError:
            return None

    def size(self):
        return len(json.dumps(self.values))

database = Database()

