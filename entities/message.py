from entities.chat import Chat
from entities.photoSize import PhotoSize
from entities.sticker import Sticker
from entities.user import User
from .entity import Entity

class Message(Entity):
    def __init__(self, message):
        Entity.__init__(self, message)
        if message is None: return

        self.id = message.get('message_id')
        self.user = User(message.get('from'))
        self.date = message.get('date')
        self.chat = Chat(message.get('chat'))

        self.text = message.get('text')
        self.reply_to_message = Message(message.get('reply_to_message'))

        if message.get('forward_from') is not None:
            self.forward_from = User(message.get('forward_from'))
        elif message.get('forward_from_chat') is not None:
            self.forward_from = Chat(message.get('forward_from_chat'))
        else:
            self.forward_from = None

        self.forward_from_message_id = message.get('forward_from_message_id')
        self.forward_date = message.get('forward_date')

        self.new_chat_member = User(message.get('new_chat_member'))
        self.left_chat_member = User(message.get('left_chat_member'))

        self.sticker = Sticker(message.get('sticker'))

        photoSizes = message.get('photo')
        if photoSizes is not None:
            self.photo = [PhotoSize(p) for p in photoSizes]
        else:
            self.photo = None

    def isSpecial(self):
        specials = ['new_chat_members', 'left_chat_member', 'new_chat_title', 'new_chat_photo', 'pinned_message', 'new_chat_members']

        for special in specials:
            if special in self.raw:
                return True

        return False

    @property
    def command(self):
        if self.text is None: return None
        if len(self.text.strip()) == 1: return None

        if self.text[:2] == '! ':
            return self.arguments[1].lower()

        if self.text[0] not in ['!','/']: return None

        cmd = self.arguments[0][1:].lower()
        if '@' in cmd:
            cmd = cmd.split('@')[0]
        return cmd

    @property
    def arguments(self):
        if self.text is None: return []
        args = self.text.split(' ')

        if len(args) > 1:
            if args[1] == ' ':
                args.pop(1)

        return args

