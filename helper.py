import hashlib, base64

from constants import COMMANDS
from database import database

def getRealCommand(alias):
    if alias in COMMANDS: return alias

    aliases = database.get('aliases', {})

    if alias not in aliases:
        return None

    real = aliases[alias]

    if real in COMMANDS:
        return real
    else:
        return getRealCommand(real)


def customHash(string):
    sha256 = hashlib.sha256(str(string).encode('utf-8'))
    binary = sha256.digest()[:8]
    b64 = base64.b64encode(binary)
    return b64.decode('utf-8')

def sanitize(message):
    message = message.replace('&', '&amp;')
    message = message.replace('<', '&lt;')
    message = message.replace('>', '&gt;')
    message = message.replace('"', '&quot;')

    return message

def button(text, data):
    return {'text': text,
            'callback_data': data}

def minutesDisplay(remainder, units=None):
    if units is None:
        units = [
            ('year',       525960),
            ('month',       43830),
            ('week',        10080),
            ('day',          1440),
            ('hour',           60),
            ('minute',          1),
            ('second',     60**-1)]

    if not units: return ""

    currentUnit = units.pop(0)
    name = currentUnit[0]
    size = currentUnit[1]

    numberOfUnits = remainder // size
    remainder -= numberOfUnits*size

    restOfMessage = minutesDisplay(remainder, units)

    if numberOfUnits == 0:
        return restOfMessage

    message = "%d %s%s" % (numberOfUnits, name, "s" if numberOfUnits > 1 else "")

    if restOfMessage == "": return message

    if restOfMessage.count("and") == 0:
        return message + " and " + restOfMessage
    else:
        return message + ", " + restOfMessage