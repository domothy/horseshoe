import json, traceback, bot

from helper import sanitize, getRealCommand

from constants import *

# All this ugliness below has the purpose of making rule-making easier with simple annotations

class rule:
    def __init__(self, kind, func=None):
        if callable(kind):
            self.func = kind
            self.kind = 'message'
        else:
            self.func = func
            self.kind = kind

    def __call__(self, entity):
        if self.func:
            if entity.kind != self.kind: return False

            return self.func(entity) is None
        else:
            return rule(func=entity, kind=self.kind)

class interceptor(rule):
    pass

def chat(func):
    def f(message):
        if message.chat.id != CHAT_ID: return False

        return func(message)

    return f

def private(func):
    def f(message):
        if message.chat.type != 'private': return False
        if message.user.id == BOT_ID: return False

        return func(message)
    return f

def command(commandName):
    def inner(func):
        def f(message):
            if message.command == commandName or getRealCommand(message.command) == commandName:
                return func(message)

            return False
        return f

    COMMANDS.append(commandName)

    return inner

def data(value):
    def inner(func):
        def f(callback):
            if callback.data == value:
                return func(callback)
            return False
        return f

    return inner

def admin(func):
    def f(message):
        if message.user.id != DOM_ID: return False
        return func(message)

    return f

def reply(func):
    def f(message):
        if message.reply_to_message.isNone: return False

        return func(message)

    return f

def text(func):
    def f(message):
        if message.text is None: return False

        return func(message)

    return f

def noParam(func):
    # noinspection PyUnusedLocal
    def f(*args):
        return func()

    return f

def debug(func):
    def f(update):
        try:
            return func(update)
        except:
            print('Exception raised - Reporting error')
            print(traceback.format_exc())
            error  = json.dumps(update.raw, indent=1)
            error += '\n————————————\n'
            error += traceback.format_exc()

            error = sanitize(error)

            bot.sendMessage(chat_id=DOM_ID,
                            text='<pre>' + error + '</pre>',
                            parse_mode='HTML')

            return False

    return f