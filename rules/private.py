import bot

from decorators import rule, private, reply, admin, command
from entities import Message
from database import database

from constants import *

@rule
@private
def forwardToDom(message):
    if message.chat.id == DOM_ID: return False
    if message.command == 'start': return False

    ignored = database.get('ignored', [])
    if message.chat.id in ignored:
        return

    req = bot.forwardMessage(chat_id = DOM_ID,
                             from_chat_id = message.chat.id,
                             message_id = message.id)

    newMessage = Message(req['result'])

    if message.forward_from is not None:
        bot.sendMessage(chat_id = DOM_ID,
                        text = 'This was actually sent by <a href="tg://user?id={0}">{1}</a>'.format(message.user.id, message.user.first_name),
                        reply_to_message_id=newMessage.id,
                        parse_mode='HTML')

@rule
@private
@reply
@admin
@command('r')
def proxyReply(message):
    text = " ".join(message.arguments[1:])

    forward = message.reply_to_message.forward_from
    if forward is None: return False

    bot.sendMessage(chat_id=forward.id,
                    text=text,
                    parse_mode='markdown')

@rule
@private
@reply
@admin
@command('ignore')
def proxyReply(message):
    text = " ".join(message.arguments[1:])

    forward = message.reply_to_message.forward_from
    if forward is None: return False

    ignored = database.get('ignored', [])
    ignored.append(forward.id)
    database.save()