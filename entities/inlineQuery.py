from entities.entity import Entity
from entities.user import User

class InlineQuery(Entity):
    def __init__(self, query):
        Entity.__init__(self, query)
        if query is None: return

        self.kind = 'inline_query'

        self.id = query.get('id')
        self.author = User(query.get('from'))
        self.query = query.get('query')
        self.offset = query.get('offset')