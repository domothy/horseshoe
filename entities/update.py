from entities.callbackQuery import CallbackQuery
from .inlineQuery import InlineQuery
from .message import Message
from .entity import Entity

class Update(Entity):
    def __init__(self, update):
        Entity.__init__(self, update)

        self.id = update.get('update_id')

        self.type = list(update.keys())[1]

        if self.type in ["message", "edited_message", "channel_post", "edited_channel_post"]:
            self.body = Message(update.get(self.type))
            self.body.kind = self.type
        elif self.type =='inline_query':
            self.body = InlineQuery(update.get('inline_query'))
        elif self.type == 'callback_query':
            self.body = CallbackQuery(update.get('callback_query'))

