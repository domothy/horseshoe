import time, bot

from decorators import rule, chat, command, reply, noParam
from helper import minutesDisplay, customHash, getRealCommand
from database import database

from constants import *

@rule
@chat
@command('qotcount')
def qotcount(message):
    qotCount = database.get('qotcount', 0)

    bot.sendMessage(chat_id=CHAT_ID,
                    text='<pre>Qot has been said {:,} times.</pre>'.format(qotCount),
                    parse_mode = 'HTML',
                    reply_to_message_id = message.id)

@rule
@chat
@reply
@command('pin')
def pin(message):
    parent = message.reply_to_message

    if parent.command is not None: return False
    if parent.isSpecial(): return False
    if parent.user.id == message.user.id: return False

    bot.pinChatMessage(chat_id = CHAT_ID,
                       message_id = parent.id,
                       disable_notification = True)

    database.set('lastpin', message.reply_to_message.id)

@rule
@chat
@command('unpin')
@noParam
def unpin():
    bot.unpinChatMessage(chat_id = CHAT_ID)

@rule
@chat
@command('repin')
@noParam
def repin():
    lastPin = database.get('lastpin', -1)

    bot.pinChatMessage(chat_id = CHAT_ID,
                       message_id = lastPin,
                       disable_notification = True)

@rule
@chat
@command('mute')
def mute(message):
    try:
        duration = float(message.arguments[1])
    except:
        duration = 5.0

    msg = '<pre>Muted for {0}.</pre>'.format(minutesDisplay(duration))

    if duration <  1: duration = 1
    if duration > 60: duration = 60

    bot.restrictChatMember(chat_id = CHAT_ID,
                           user_id = message.user.id,
                           can_send_messages = False,
                           until_date = message.date + duration * 60)

    bot.sendMessage(chat_id = CHAT_ID,
                    reply_to_message_id = message.id,
                    text = msg,
                    parse_mode = 'HTML')

@rule
@chat
@command('messagecount')
def messageCount(message):
    nb = message.id

    if message.reply_to_message.isDefined:
        nb -= message.reply_to_message.id

    bot.sendMessage(chat_id = CHAT_ID,
                    reply_to_message_id = message.id,
                    text = '<pre>{:,}</pre>'.format(nb),
                    parse_mode = 'HTML')

@rule
@chat
@reply
@command('bestof')
def bestof(message):
    parent = message.reply_to_message
    previousBestOf = database.get('bestof-previous', [])

    bestofd = False

    msg = 'Bestof\'d'

    if parent.id in previousBestOf:
        msg = 'That has already been bestof\'d before.'
    elif parent.user.id == message.user.id:
        msg = 'You can\'t bestof your own thing.'
    elif parent.forward_from is not None:
        msg = 'Forwarded messages can\'t be bestof\'d.'
    elif parent.isSpecial():
        msg = 'This obviously can\'t be bestof\'d, idiot.'
    elif parent.user.id == BOT_ID:
        if 'Ouija' in parent.text or 'roulette' in parent.text:
            bestofd = True
        else:
            msg = 'The bot\'s messages can\'t be bestof\'d.'
    elif parent.command is not None:
        msg = 'Commands can\'t be bestof\'d.'
    else:
        bestofd = True

    if bestofd:
        previousBestOf.append(parent.id)
        database.save()

        bot.forwardMessage(chat_id = BESTOF_CHANNEL_ID,
                           from_chat_id = CHAT_ID,
                           message_id = parent.id)


    bot.sendMessage(chat_id = CHAT_ID,
                    reply_to_message_id = message.id,
                    text = '<pre>{0}</pre>\n\n— <a href="https://t.me/joinchat/AAAAAE5c4k7ZqM1aLqx8Gw">View channel</a>'.format(msg),
                    parse_mode = 'HTML',
                    disable_web_page_preview = True)

@rule
@chat
@reply
@command('pic')
def changePic(message):
    if message.reply_to_message.photo is None: return False

    photoId = message.reply_to_message.photo[-1].file_id
    hashed = customHash(photoId)

    previousPics = database.get('pics-previous', [])
    nextTime = database.get('pics-nexttime', 0)

    if hashed in previousPics:
        bot.sendMessage(chat_id = CHAT_ID,
                        reply_to_message_id = message.id,
                        text = '<pre>This pic has already been used before.</pre>',
                        parse_mode = 'HTML')
        return

    if message.date < nextTime:
        delta = (nextTime - message.date) / 60.0

        bot.sendMessage(chat_id = CHAT_ID,
                        reply_to_message_id = message.id,
                        text = '<pre>Wait {0} before changing the pic.</pre>'.format(minutesDisplay(delta)),
                        parse_mode = 'HTML')
        return

    previousPics.append(hashed)
    database.set('pics-nexttime', message.date + 5 * 60)

    req = bot.getFile(file_id = photoId)
    filePath = req['result']['file_path']

    bot.download(open('temp', 'wb'), filePath)

    bot.setChatPhoto(chat_id = CHAT_ID,
                     photo = open('temp', 'rb'))


@rule
@chat
@command('leave')
def leave(message):
    bot.sendMessage(chat_id = CHAT_ID,
                    reply_to_message_id = message.id,
                    text = '<pre>Bang</pre>',
                    parse_mode = 'HTML')

    time.sleep(2)

    bot.kickChatMember(chat_id = CHAT_ID,
                       user_id = message.user.id,
                       until_date = time.time() + 40)


@rule
@chat
@command('alias')
def alias(message):
    try:
        realCommand = message.arguments[1]
        aliasName = message.arguments[2]

        if aliasName in COMMANDS:
            bot.sendMessage(chat_id = CHAT_ID,
                            reply_to_message_id = message.id,
                            text = '<pre>You can\'t overwrite an existing command.</pre>',
                            parse_mode = 'HTML')
            return

        if realCommand not in COMMANDS and getRealCommand(realCommand) not in COMMANDS:
            bot.sendMessage(chat_id = CHAT_ID,
                            reply_to_message_id = message.id,
                            text = '<pre>That command doesn\'t exist.</pre>',
                            parse_mode = 'HTML')
            return


        aliases = database.get('aliases', {})

        oldAlias = "" if aliasName not in aliases else aliases[aliasName]

        aliases[aliasName] = realCommand
        database.save()

        try:
            getRealCommand(aliasName)
        except RecursionError:
            aliases[aliasName] = oldAlias
            bot.sendMessage(chat_id = CHAT_ID,
                            reply_to_message_id = message.id,
                            text = '<pre>That\'s too much recursion.</pre>',
                            parse_mode = 'HTML')

    except IndexError:
        return

@rule
@chat
@command('ilya')
def ilya(message):
    file_id = 'DQADAQADPAADRIJgRZeXQbf9Ju7nAg'

    if message.reply_to_message.isDefined:
        bot.deleteMessage(chat_id = CHAT_ID,
                          message_id = message.id)
        bot.sendVideoNote(chat_id = CHAT_ID,
                          reply_to_message_id = message.reply_to_message.id,
                          video_note = file_id)
    else:
        bot.sendVideoNote(chat_id = CHAT_ID,
                          reply_to_message_id = message.id,
                          video_note = file_id)