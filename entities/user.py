from helper import sanitize
from .entity import Entity

class User(Entity):
    def __init__(self, user):
        Entity.__init__(self, user)
        if user is None: return

        self.id = user.get('id')
        self.hash = str(user.get('id'))

        self.is_bot = user.get('is_bot')
        self.first_name = user.get('first_name')
        self.last_name = user.get('last_name')
        self.username = user.get('username')

    @property
    def display(self):
        if self.username is None:
            return '<a href="tg://user?id={0}">{1}</a>'.format(self.id, sanitize(self.first_name))
        else:
            return '@' + self.username

    @property
    def name(self):
        fullname = self.first_name

        if self.last_name is not None:
            fullname += " " + self.last_name

        return sanitize(fullname)
