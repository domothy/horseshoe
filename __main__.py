import bot, traceback, sys

from time import sleep
from handler import handle
from rules import debug
from database import database

def safeUpdateFetcher(offset):
    while True:
        try:
            json = bot.getUpdates(offset=offset)

            return json['result']
        except:
            print(traceback.format_exc())
            sleep(5)

def main():
    offset = database.get('offset', 0)

    while True:
        updates = safeUpdateFetcher(offset)

        for update in updates:
            offset = update['update_id'] + 1
            handle(update)

        database.set('offset', offset)

        sleep(1)

if __name__ == "__main__":

    if len(sys.argv) > 1:
        if sys.argv[1] == 'rebooted':
            debug.inform()

    main()
