class Entity:
    def __init__(self, raw):
        self.raw = raw

    @property
    def isDefined(self):
        return self.raw is not None

    @property
    def isNone(self):
        return self.raw is None

    def __str__(self):
        return str(self.raw)

    def __repr__(self):
        return self.raw

    def __getattribute__(self, name):
        try:
            return object.__getattribute__(self, name)
        except AttributeError:
            return None
