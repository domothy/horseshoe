from .entity import Entity

class Sticker(Entity):
    def __init__(self, sticker):
        Entity.__init__(self, sticker)
        if sticker is None: return

        self.file_id = sticker.get('file_id')