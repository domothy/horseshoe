from .entity import Entity

class Chat(Entity):
    def __init__(self, chat):
        Entity.__init__(self, chat)
        if chat is None: return

        self.id = chat.get('id')
        self.type = chat.get('type')