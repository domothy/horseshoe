import threading, rules

from entities import Update
from decorators import debug

def handle(rawUpdate):
    update = Update(rawUpdate)

    thread = threading.Thread(target=runRules, args=[update])
    thread.start()

@debug
def runRules(update):
    print(update)

    body = update.body
    for funcName in dir(rules):
        attr = getattr(rules, funcName)

        if isinstance(attr, rules.interceptor):
            if attr(body):
                print("Interceptor match: {0}".format(funcName))
                return

    for funcName in dir(rules):
        attr = getattr(rules, funcName)

        if isinstance(attr, rules.rule) and not isinstance(attr, rules.interceptor):
            if attr(body):
                print("Rule match: {0}".format(funcName))
                break

