import requests, json, io

from handler import handle
from constants import *

lastResponses = {'last': {}}

def download(fileObj, filePath):
    url = 'https://api.telegram.org/file/bot{0}/{1}'.format(TOKEN, filePath)

    r = requests.get(url)
    fileObj.write(r.content)
    fileObj.close()

def __getattr__(method):
    def f(**kw):
        url = 'https://api.telegram.org/bot{0}/{1}'.format(TOKEN, method)

        payload = {}
        files = {}

        for key in kw:
            item = kw[key]

            if isinstance(item, io.IOBase):
                files[key] = item.read()
                item.close()
            else:
                payload[key] = item

        r = requests.post(url, data = payload, files = files)
        result = json.loads(r.text)

        if method == 'sendMessage':
            if result['ok']:
                update = {'update_id': 0, 'message': result['result']}
                handle(update)

        if method != 'getUpdate':
            lastResponses[method] = result

        return result

    return f

