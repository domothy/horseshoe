import bot

from decorators import interceptor, chat, text

from database import database
from constants import *
from helper import getRealCommand


@interceptor
@chat
def mute_during_shutdown(message):
    if message.user.id == DOM_ID: return False
    if message.date > database.get('shutdown-until', 0): return False

    muteUntil = database.get('shutdown-until', 0)
    if muteUntil - message.date < 60:
        muteUntil = message.date + 60

    bot.deleteMessage(chat_id = CHAT_ID,
                      message_id = message.id)

    bot.restrictChatMember(chat_id = CHAT_ID,
                           user_id = message.user.id,
                           can_send_messages = False,
                           until_date = muteUntil)

    mutedUsers = database.get('shutdown-muted', [])
    mutedUsers.append(message.user.id)
    database.save()

    return True

@interceptor
@chat
@text
def qot_detector(message):
    if message.user.id == BOT_ID: return False

    txt = message.text.lower()
    if 'qot' not in txt: return False
    if '!qotcount' in txt: return False

    qotCount = database.get('qotcount', 0)
    qotCount += txt.count('qot')

    database.set('qotcount', qotCount)

    return False

@interceptor
@chat
def milestone(message):
    every = 10000

    if message.id % every != 0: return False

    bot.sendMessage(chat_id = CHAT_ID,
                    reply_to_message_id = message.id,
                    text = '<pre>This is the {:,}th message in the horseshoe!</pre>'.format(message.id),
                    parse_mode = 'HTML')

    return False

@interceptor
@chat
@text
def noSlashCommands(message):
    if message.text[0] != '/': return False
    if getRealCommand(message.command) == 'leave': return False

    bot.deleteMessage(chat_id = CHAT_ID,
                      message_id = message.id)


@interceptor
@chat
def noStickers(message):
    if message.sticker.isNone:
        return False
    
    if message.sticker.file_id != 'CAADBAADmwIAAjY2UQyEWT6LiJNHngI':
        bot.deleteMessage(chat_id = CHAT_ID,
                          message_id = message.id)

    bot.restrictChatMember(chat_id = CHAT_ID,
                           user_id = message.user.id,
                           can_add_web_page_previews = True,
                           can_send_other_messages = False,
                           until_date = message.date + 10*60)
    return True

@interceptor
@chat
def noBots(message):
    if message.new_chat_member.isNone: return False

    user = message.new_chat_member

    if user.is_bot:
        bot.kickChatMember(chat_id = CHAT_ID,
                           user_id = user.id,
                           until_date = message.date + 60)
        bot.sendMessage(chat_id = CHAT_ID,
                        reply_to_message_id = message.id,
                        text = '<pre>I am the only bot here.</pre>',
                        parse_mode = 'HTML')
        return True

    return False

@interceptor
@chat
def noSpam(message):
    if message.user.id == BOT_ID: return False

    userId = message.user.id
    date = message.date

    previousMessages = database.get('spam-prevMsgs', {})

    if userId not in previousMessages:
        previousMessages[userId] = []

    previousMessages = previousMessages[userId]
    previousMessages.append(date)

    nbMessages = len(previousMessages)
    if nbMessages > 50:
        previousMessages = previousMessages[:50]

    previousMessages.sort()

    earliest = previousMessages[0]
    latest = previousMessages[-1]

    if nbMessages < 10:
        database.save()
        return False

    maxRate = 3

    if nbMessages >= 25:
        maxRate = 2.5
    if nbMessages >= 40:
        maxRate = 2

    delta = latest - earliest

    if delta == 0 or nbMessages / delta > maxRate:

        mutedUntil = message.date + 60 * 2

        mutedUsers = database.get('spam-muted', {})
        mutedUsers[userId] = mutedUntil

        database.save()

        bot.restrictChatMember(chat_id = CHAT_ID,
                               user_id = message.user.id,
                               can_send_messages = False,
                               until_date = mutedUntil)

        bot.sendMessage(chat_id = CHAT_ID,
                        text = '{0} muted for spamming\n\n<pre>{1} messages over {2} seconds</pre>'.format(message.user.display, nbMessages, delta),
                        parse_mode = 'HTML')


        return True

    return False

@interceptor
@chat
def supposedToBeMuted(message):
    userId = message.user.id
    date = message.date
    mutedUsers = database.get('spam-muted', {})

    if userId in mutedUsers:
        if date < mutedUsers[userId]:
            bot.deleteMessage(chat_id = CHAT_ID,
                              message_id = message.id)
            return True

    return False