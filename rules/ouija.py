import bot, threading, json, time

from decorators import rule, chat, command, reply, text
from constants import *

class Game:
    def __init__(self, messageId):
        self.message = ""
        self.lock = threading.Lock()
        self.lastUpdate = 0
        self.messageId = messageId
        self.over = False

    def update(self):
        bot.editMessageText(chat_id = CHAT_ID,
                            message_id = self.messageId,
                            text = '<b>Ouija board</b>\n\n<pre>{0}</pre>'.format(self.message),
                            parse_mode = 'HTML',
                            reply_markup = "{}" if self.over else KEYBOARD)
buttonRows = []

for row in ['ABCDEFG', 'HIJKLMN', 'OPQRSTU', '\nVWXYZ ']:
    buttonRow = []

    for letter in row:
        display = letter
        if letter == '\n': display='⏎'

        buttonRow.append({
            'text': display,
            'callback_data': 'ouija/{0}'.format(letter)
        })

    buttonRows.append(buttonRow)

KEYBOARD = json.dumps({'inline_keyboard': buttonRows})

games = {}

@rule
@chat
@command('ouija')
def ouija(message):
    bot.sendMessage(chat_id = CHAT_ID,
                    reply_to_message_id = message.id,
                    text = '<b>Ouija board</b>\n\n<i>Some spooky shit is about to go down</i>',
                    parse_mode = 'HTML',
                    reply_markup=KEYBOARD)

@rule('callback_query')
@command('ouija')
def updateOuija(callbackQuery):
    print(callbackQuery)

    chosenLetter = callbackQuery.arguments[1]
    messageId = callbackQuery.message.id

    if messageId not in games:
        games[messageId] = Game(messageId)

    game = games[messageId]

    game.lock.acquire()

    try:
        game.message += chosenLetter

        if game.lastUpdate < time.time() - 2 or chosenLetter in [' ', '\n']:
            game.update()
            game.lastUpdate = time.time()

    finally:
        game.lock.release()

@rule
@chat
@reply
@text
def ouijaGoodbye(message):
    if message.text[:7].lower() != "goodbye": return False

    parentid = message.reply_to_message.id

    if parentid not in games: return False

    game = games[parentid]

    game.lock.acquire()
    try:
        game.over = True
        game.update()
    finally:
        game.lock.release()

    return True